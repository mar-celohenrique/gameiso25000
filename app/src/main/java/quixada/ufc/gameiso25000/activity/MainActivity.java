package quixada.ufc.gameiso25000.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import quixada.ufc.gameiso25000.R;

public class MainActivity extends AppCompatActivity{

    private Button buttonIniciar;
    private Button buttonSair;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        iniciarBotoes();
    }

    public void iniciarBotoes(){
        buttonIniciar = (Button) findViewById(R.id.buttonIniciar);
        capturaClickBotaoIniciar();
        buttonSair = (Button) findViewById(R.id.buttonSair);
        capturaClickBotaoSair();

    }

    private void capturaClickBotaoIniciar() {
        buttonIniciar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), FaseActivity.class);
                startActivity(intent);
            }
        });
    }

    private void capturaClickBotaoSair(){
        buttonSair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogConfirmacaoSaida().show();
            }
        });
    }

    private Dialog dialogConfirmacaoSaida(){
        return new AlertDialog.Builder(this).setTitle("Confirmação de saída da aplicação")
                .setMessage("Deseja realmente sair da aplicação?")
                .setPositiveButton("Sim", confirmacaoPosisitiva())
                .setNegativeButton("Não", confirmacaoNegativa()).create();
    }

    private android.content.DialogInterface.OnClickListener confirmacaoPosisitiva() {
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        };
    }

    private android.content.DialogInterface.OnClickListener confirmacaoNegativa() {
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                return;
            }
        };
    }

    @Override
    public void onBackPressed() {
        dialogConfirmacaoSaida().show();
    }
}
