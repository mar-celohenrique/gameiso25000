package quixada.ufc.gameiso25000.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import quixada.ufc.gameiso25000.R;
import quixada.ufc.gameiso25000.dao.BaseDeDadosPerguntas;

public class ResultadoActivity extends AppCompatActivity {

    private ArrayList<String> listaDeRespostas = new ArrayList<>();
    private final BaseDeDadosPerguntas baseDeDados = new BaseDeDadosPerguntas(this);
    private TextView tvPontos;
    private TextView tvTotal;
    private int fase = 0;
    private int pontuacao = 0;
    private int respondidas = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultado);

        TextView t = (TextView) findViewById(R.id.textResultado);
        tvPontos = (TextView) findViewById(R.id.textPontos);
        tvTotal = (TextView) findViewById(R.id.textTotal);

        Bundle b = getIntent().getExtras();
        pontuacao = b.getInt("acertos");
        respondidas = b.getInt("respondidas");
        double media = 0.7;
        fase = b.getInt("fase");


        b.putInt("fase", fase);

        listaDeRespostas = b.getStringArrayList("listaDeRespostas");


        Button btnDone = (Button) findViewById(R.id.btnInicio);
        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ResultadoActivity.this, MainActivity.class); // essa é activity Inicial do app
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); // adiciona a flag para a intent
                startActivity(intent);
            }
        });

        Button btnRespostas = (Button) findViewById(R.id.btnRespostas);

        if (pontuacao < (respondidas * media)) {
            btnRespostas.setEnabled(false);
            t.setText("REPROVADO!");
        } else {
            t.setText("APROVADO!");

            btnRespostas.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ResultadoActivity.this, RespostasActivity.class);
                    Bundle b = new Bundle();
                    b.putInt("fase", fase);
                    b.putStringArrayList("listaDeRespostas", listaDeRespostas);
                    intent.putExtras(b);
                    startActivity(intent);
                }
            });
            baseDeDados.atualizarFase(true, fase);
        }
        adicionarResultado();
    }

    public void adicionarResultado() {
        tvPontos.setText("" + pontuacao);
        tvTotal.setText("" + respondidas);
    }
}
