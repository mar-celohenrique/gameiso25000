package quixada.ufc.gameiso25000.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import quixada.ufc.gameiso25000.R;
import quixada.ufc.gameiso25000.dao.BaseDeDadosPerguntas;

public class FaseActivity extends AppCompatActivity implements View.OnClickListener {

    private Button buttonFase1;
    private Button buttonFase2;
    private Button buttonFase3;
    private Button buttonFase4;
    private Button buttonFase5;
    private Button buttonFase6;
    private final BaseDeDadosPerguntas baseDeDados = new BaseDeDadosPerguntas(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fase);
        iniciarBotoes();
    }

    public void iniciarBotoes() {
        buttonFase1 = (Button) findViewById(R.id.buttonFase1);
        buttonFase1.setOnClickListener(this);
        buttonFase2 = (Button) findViewById(R.id.buttonFase2);
        buttonFase2.setOnClickListener(this);
        buttonFase3 = (Button) findViewById(R.id.buttonFase3);
        buttonFase3.setOnClickListener(this);
        buttonFase4 = (Button) findViewById(R.id.buttonFase4);
        buttonFase4.setOnClickListener(this);
        buttonFase5 = (Button) findViewById(R.id.buttonFase5);
        buttonFase5.setOnClickListener(this);

        buttonFase6 = (Button) findViewById(R.id.buttonFase6);
        buttonFase6.setOnClickListener(this);

        /*if (!baseDeDados.listarFases().get(0).isCompleta()) {
            buttonFase2.setEnabled(false);
        }
        if (!baseDeDados.listarFases().get(1).isCompleta()) {
            buttonFase3.setEnabled(false);
        }
        if (!baseDeDados.listarFases().get(2).isCompleta()) {
            buttonFase4.setEnabled(false);
        }
        if (!baseDeDados.listarFases().get(3).isCompleta()) {
            buttonFase5.setEnabled(false);
        }
        if (!baseDeDados.listarFases().get(4).isCompleta()) {
            buttonFase6.setEnabled(false);
        }*/
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == buttonFase1.getId()) {
            Bundle b = new Bundle();
            b.putInt("id", baseDeDados.listarFases().get(0).getId());
            Intent intent = new Intent(this, PerguntaActivity.class);
            intent.putExtras(b);
            Toast.makeText(getApplicationContext(), baseDeDados.contarNumeroPerguntas(baseDeDados.listarFases().get(0).getId())+"", Toast.LENGTH_SHORT).show();
            startActivity(intent);
        }

        if (v.getId() == buttonFase2.getId()) {
            Bundle b = new Bundle();
            b.putInt("id", baseDeDados.listarFases().get(1).getId());
            Intent intent = new Intent(this, PerguntaActivity.class);
            intent.putExtras(b);
            Toast.makeText(getApplicationContext(), baseDeDados.contarNumeroPerguntas(baseDeDados.listarFases().get(1).getId())+"", Toast.LENGTH_SHORT).show();

            startActivity(intent);
        }

        if (v.getId() == buttonFase3.getId()) {

            Bundle b = new Bundle();
            b.putInt("id", baseDeDados.listarFases().get(2).getId());
            Intent intent = new Intent(this, PerguntaActivity.class);
            intent.putExtras(b);
            Toast.makeText(getApplicationContext(), baseDeDados.contarNumeroPerguntas(baseDeDados.listarFases().get(2).getId())+"", Toast.LENGTH_SHORT).show();

            startActivity(intent);
        }

        if (v.getId() == buttonFase4.getId()) {
            Bundle b = new Bundle();
            b.putInt("id", baseDeDados.listarFases().get(3).getId());
            Intent intent = new Intent(this, PerguntaActivity.class);
            intent.putExtras(b);
            Toast.makeText(getApplicationContext(), baseDeDados.contarNumeroPerguntas(baseDeDados.listarFases().get(3).getId())+"", Toast.LENGTH_SHORT).show();

            startActivity(intent);
        }
        if (v.getId() == buttonFase5.getId()) {
            Bundle b = new Bundle();
            b.putInt("id", baseDeDados.listarFases().get(4).getId());
            Intent intent = new Intent(this, PerguntaActivity.class);
            intent.putExtras(b);
            Toast.makeText(getApplicationContext(), baseDeDados.contarNumeroPerguntas(baseDeDados.listarFases().get(4).getId())+"", Toast.LENGTH_SHORT).show();

            startActivity(intent);
        }
        if (v.getId() == buttonFase6.getId()) {
            Bundle b = new Bundle();
            b.putInt("id", baseDeDados.listarFases().get(5).getId());
            Intent intent = new Intent(this, DragActivity.class);
            intent.putExtras(b);
            startActivity(intent);

        }
    }
}
