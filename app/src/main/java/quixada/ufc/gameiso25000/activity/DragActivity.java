package quixada.ufc.gameiso25000.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.DragEvent;
import android.view.View;
import android.view.View.OnDragListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import quixada.ufc.gameiso25000.R;
import quixada.ufc.gameiso25000.dao.BaseDeDadosPerguntas;
import quixada.ufc.gameiso25000.model.Caracteristica;
import quixada.ufc.gameiso25000.model.Subcaracteristica;

public class DragActivity extends AppCompatActivity implements View.OnClickListener, OnLongClickListener, OnDragListener {

    private Button button;
    private Caracteristica caracteristicaAtual;
    private List<Caracteristica> caracteristicas;
    private int idCaracteristica = 0;

    private List<Subcaracteristica> pertencentes;
    private List<Subcaracteristica> naoPertencentes;

    private ArrayList<String> listaDeRespostas;
    private int acertos = 0;
    private int respondidas = 0;
    private int fase = 0;

    private final BaseDeDadosPerguntas baseDeDadosPerguntas = new BaseDeDadosPerguntas(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drag);
        findViewById(R.id.centerRight).setOnDragListener(this);
        findViewById(R.id.centerLeft).setOnDragListener(this);
        button = (Button) findViewById(R.id.contar);
        button.setOnClickListener(this);

        caracteristicas = baseDeDadosPerguntas.listarCaracteristicas();
        caracteristicaAtual = caracteristicas.get(idCaracteristica);

        listaDeRespostas = new ArrayList<>();
        pertencentes = new ArrayList<>();
        naoPertencentes = new ArrayList<>();

        Bundle b = getIntent().getExtras();
        fase = b.getInt("id");

        adicionarComponentes();
    }


    //aqui a cobra fuma
    @Override
    public void onClick(View v) {
        if (v.getId() == button.getId()) {
            LinearLayout direito = (LinearLayout) findViewById(R.id.centerRight);
            if (direito.getChildCount() > 0) {
                String[] respostas = new String[direito.getChildCount()];
                for (int i = 0; i < direito.getChildCount(); i++) {
                    TextView tv = (TextView) ((LinearLayout) direito).getChildAt(i);
                    respostas[i] = tv.getText().toString();
                }
                for (int j = 0; j < respostas.length; j++) {
                    for (int k = 0; k < pertencentes.size(); k++) {
                        if (respostas[j].equalsIgnoreCase(pertencentes.get(k).getNome())) {
                            acertos++;
                        } else if(!respostas[j].equalsIgnoreCase(pertencentes.get(k).getNome())){
                            acertos--;
                        }else{
                            respondidas += pertencentes.size();
                        }
                    }
                }
                StringBuilder builder = new StringBuilder();
                for (String value : respostas) {
                    builder.append(value);
                    builder.append(" ");
                }
                listaDeRespostas.add(builder.toString());
                removerComponentes();
                if (caracteristicaAtual != null) {
                    if (idCaracteristica < baseDeDadosPerguntas.contarNumeroCaracteristicas()) {
                        caracteristicaAtual = caracteristicas.get(idCaracteristica);
                        adicionarComponentes();
                    } else {

                        Intent intent = new Intent(DragActivity.this, ResultadoActivity.class);

                        Bundle b = new Bundle();
                        b.putInt("acertos", acertos);
                        b.putInt("fase", fase);
                        b.putInt("respondidas", respondidas);
                        intent.putStringArrayListExtra("listaDeRespostas", listaDeRespostas);
                        intent.putExtras(b);
                        startActivity(intent);
                        finish();

                    }
                }
            } else {
                Toast.makeText(getApplicationContext(), "Arraste primeiro!", Toast.LENGTH_SHORT).show();
            }

        }

    }

    @Override
    public boolean onDrag(View v, DragEvent event) {
        int action = event.getAction();

        switch (action) {
            case DragEvent.ACTION_DRAG_STARTED:
                if (event.getClipDescription().hasMimeType(ClipDescription.MIMETYPE_TEXT_PLAIN)) {
                    return (true);
                }
                return (false);
            case DragEvent.ACTION_DRAG_ENTERED:
                break;
            case DragEvent.ACTION_DRAG_LOCATION:

                break;
            case DragEvent.ACTION_DRAG_EXITED:
                break;
            case DragEvent.ACTION_DROP:
                View view = (View) event.getLocalState();
                ViewGroup owner = (ViewGroup) view.getParent();
                owner.removeView(view);
                LinearLayout container = (LinearLayout) v;
                container.addView(view);
                view.setVisibility(View.VISIBLE);
                break;
            case DragEvent.ACTION_DRAG_ENDED:
                break;
        }

        return true;
    }

    @Override
    public boolean onLongClick(View v) {
        ClipData data = ClipData.newPlainText("simple_text", "text");
        View.DragShadowBuilder sb = new View.DragShadowBuilder(v);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            v.startDragAndDrop(data, sb, v, 0);
        } else {
            v.startDrag(data, sb, v, 0);
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        dialogConfirmacaoSaida().show();
    }

    private Dialog dialogConfirmacaoSaida() {
        return new AlertDialog.Builder(this).setTitle("Confirmação")
                .setMessage("Deseja realmente sair do jogo atual?")
                .setPositiveButton("Sim", confirmacaoPosisitiva())
                .setNegativeButton("Não", confirmacaoNegativa()).create();
    }

    private android.content.DialogInterface.OnClickListener confirmacaoPosisitiva() {
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        };
    }

    private android.content.DialogInterface.OnClickListener confirmacaoNegativa() {
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                return;
            }
        };
    }

    public void adicionarComponentes() {
        TextView tvCaracteristica = (TextView) findViewById(R.id.tvDragCaracteristica);
        tvCaracteristica.setText(caracteristicaAtual.getNome());
        LinearLayout layout = (LinearLayout) findViewById(R.id.centerLeft);
        LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        pertencentes = baseDeDadosPerguntas.listarSubcaracteristicasPertencentesCaracteristica(caracteristicaAtual.getId());
        naoPertencentes = baseDeDadosPerguntas.sortearSubcaracteristicas(baseDeDadosPerguntas.listarSubcaracteristicasNaoPertencentesCaracteristica(caracteristicaAtual.getId()));
        for (Subcaracteristica s : pertencentes) {
            TextView pertencente = new TextView(this);
            pertencente.setLayoutParams(lparams);
            pertencente.setText(s.getNome());
            pertencente.setOnLongClickListener(this);
            pertencente.setTextSize(17);
            layout.addView(pertencente);
        }
        for (Subcaracteristica s : naoPertencentes) {
            TextView tv = new TextView(this);
            tv.setLayoutParams(lparams);
            tv.setText(s.getNome());
            tv.setOnLongClickListener(this);
            tv.setTextSize(17);
            layout.addView(tv);
        }
        idCaracteristica++;
    }

    public void removerComponentes() {
        LinearLayout esquerdo = (LinearLayout) findViewById(R.id.centerLeft);
        LinearLayout direito = (LinearLayout) findViewById(R.id.centerRight);
        esquerdo.removeAllViews();
        direito.removeAllViews();
    }

}
