package quixada.ufc.gameiso25000.activity;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import quixada.ufc.gameiso25000.R;
import quixada.ufc.gameiso25000.dao.BaseDeDadosPerguntas;
import quixada.ufc.gameiso25000.model.Caracteristica;
import quixada.ufc.gameiso25000.model.Pergunta;

public class RespostasActivity extends AppCompatActivity {

    private ListView listaRespostas;

    private List<Pergunta> listaPerguntas;
    private Pergunta perguntaIterator;
    private Caracteristica caracteristicaIterator;
    private List<Caracteristica> caracteristicas;

    ArrayList<HashMap<String, Object>> valoresOriginais = new ArrayList<>();


    HashMap<String, Object> temporario = new HashMap<>();

    public static String TEXTO_QUESTAO = "nome";
    public static String RESPOSTA = "resposta";
    public static String RESPOSTA_USUARIO = "respostaUsuario";

    private CustomAdapter adapter;

    ArrayList<String> listaRespostasUsuario = new ArrayList<>();

    private int fase = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_respostas);

        listaRespostas = (ListView) findViewById(R.id.lvRespostas);

        //Initialize the database
        final BaseDeDadosPerguntas dbAdapter = new BaseDeDadosPerguntas(this);

        Bundle b = getIntent().getExtras();
        fase = b.getInt("fase");
        listaRespostasUsuario = b.getStringArrayList("listaDeRespostas");
        if (fase == 2) {
            caracteristicas = dbAdapter.listarCaracteristicas();
            for (int i = 0; i < caracteristicas.size(); i++) {
                caracteristicaIterator = caracteristicas.get(i);
                temporario = new HashMap<>();
                temporario.put(TEXTO_QUESTAO, caracteristicaIterator.getNome());
                temporario.put(RESPOSTA, dbAdapter.converterListaParaString(dbAdapter.listarSubcaracteristicasPertencentesCaracteristica(caracteristicaIterator.getId())));
                temporario.put(RESPOSTA_USUARIO, listaRespostasUsuario.get(i));

                valoresOriginais.add(temporario);

            }


        } else {
            listaPerguntas = dbAdapter.listarTodasPerguntasFase(fase);
            for (int i = 0; i < listaPerguntas.size(); i++) {
                perguntaIterator = listaPerguntas.get(i);
                temporario = new HashMap<>();
                temporario.put(TEXTO_QUESTAO, perguntaIterator.getTexto());
                temporario.put(RESPOSTA, perguntaIterator.getResposta());
                temporario.put(RESPOSTA_USUARIO, listaRespostasUsuario.get(i));

                // add the row to the ArrayList
                valoresOriginais.add(temporario);

            }
        }

        adapter = new CustomAdapter(this, R.layout.linhas_lista_respostas,
                valoresOriginais);
        listaRespostas.setAdapter(adapter);

    }

    // define your custom adapter
    private class CustomAdapter extends ArrayAdapter<HashMap<String, Object>> {
        LayoutInflater inflater;

        public CustomAdapter(Context context, int textViewResourceId,
                             ArrayList<HashMap<String, Object>> Strings) {
            super(context, textViewResourceId, Strings);
            inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        // class for caching the views in a row
        private class ViewHolder {

            TextView tvQS, tvCans, tvYouans;

        }

        ViewHolder viewHolder;

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if (convertView == null) {

                convertView = inflater.inflate(R.layout.linhas_lista_respostas, null);
                viewHolder = new ViewHolder();

                viewHolder.tvQS = (TextView) convertView.findViewById(R.id.tvTextoPerguntas);

                viewHolder.tvCans = (TextView) convertView
                        .findViewById(R.id.tvItemCorreto);
                viewHolder.tvYouans = (TextView) convertView
                        .findViewById(R.id.tvRespostaJogador);

                // link the cached views to the convertview
                convertView.setTag(viewHolder);

            } else
                viewHolder = (ViewHolder) convertView.getTag();

            viewHolder.tvQS.setText(valoresOriginais.get(position).get(TEXTO_QUESTAO)
                    .toString());

            viewHolder.tvCans.setText("Resposta Correta: " + valoresOriginais.get(position).get(RESPOSTA)
                    .toString());
            viewHolder.tvYouans.setText("Sua Resposta: " + valoresOriginais.get(position)
                    .get(RESPOSTA_USUARIO).toString());

            if (valoresOriginais.get(position).get(RESPOSTA)
                    .toString().equalsIgnoreCase(valoresOriginais.get(position)
                            .get(RESPOSTA_USUARIO).toString())) {
                viewHolder.tvYouans.setTextColor(Color.parseColor("#4caf50"));
            }

            // return the view to be displayed
            return convertView;
        }
    }

}
