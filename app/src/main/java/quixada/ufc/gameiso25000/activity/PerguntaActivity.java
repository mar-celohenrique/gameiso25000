package quixada.ufc.gameiso25000.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import quixada.ufc.gameiso25000.R;
import quixada.ufc.gameiso25000.dao.BaseDeDadosPerguntas;
import quixada.ufc.gameiso25000.model.Pergunta;

public class PerguntaActivity extends AppCompatActivity implements View.OnClickListener {

    private List<Pergunta> listaDePerguntas;
    private Pergunta perguntaAtual;

    private TextView tvTextoPergunta;
    private TextView tvPontos;
    private RadioButton itemA, itemB, itemC, itemD;
    private Button btnProximaPergunta;

    private int respostasCorretas = 0;
    private int perguntasRespondidas = 0;
    private int idPergunta = 0;

    private ArrayList<String> listaDeRespostas;
    private final BaseDeDadosPerguntas baseDeDados = new BaseDeDadosPerguntas(this);
    private int fase = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pergunta);
        Bundle b = getIntent().getExtras();
        fase = b.getInt("id");
        inicializarComponentes();
        listaDePerguntas = baseDeDados.listarTodasPerguntasFase(fase);
        perguntaAtual = listaDePerguntas.get(idPergunta);



        btnProximaPergunta.setOnClickListener(this);
        adicionarQuestaoATela();
    }

    public void inicializarComponentes() {
        tvTextoPergunta = (TextView) findViewById(R.id.tvPergunta);
        tvPontos = (TextView) findViewById(R.id.pontos);
        itemA = (RadioButton) findViewById(R.id.radioItemA);
        itemB = (RadioButton) findViewById(R.id.radioItemB);
        itemC = (RadioButton) findViewById(R.id.radioItemC);
        itemD = (RadioButton) findViewById(R.id.radioItemD);

        btnProximaPergunta = (Button) findViewById(R.id.btnProximaPergunta);

        listaDeRespostas = new ArrayList<>();
    }

    private void adicionarQuestaoATela() {
        tvPontos.setText(""+respostasCorretas);
        itemA.setChecked(false);
        itemB.setChecked(false);
        itemC.setChecked(false);
        itemD.setChecked(false);

        tvTextoPergunta.setText(perguntaAtual.getTexto());
        itemA.setText(perguntaAtual.getItemA());
        itemB.setText(perguntaAtual.getItemB());
        itemC.setText(perguntaAtual.getItemC());
        itemD.setText(perguntaAtual.getItemD());

        idPergunta++;
    }

    @Override
    public void onClick(View v) {
        RadioGroup grp = (RadioGroup) findViewById(R.id.radioGroupItens);
        RadioButton resposta = (RadioButton) findViewById(grp.getCheckedRadioButtonId());
        if (resposta != null) {
            listaDeRespostas.add("" + resposta.getText());

            if (perguntaAtual.getResposta().equalsIgnoreCase(resposta.getText().toString())) {
                respostasCorretas+=10;
                perguntasRespondidas+=10;
            } else {
                perguntasRespondidas+=10;
            }
            if (idPergunta < baseDeDados.contarNumeroPerguntas(fase)) {
                perguntaAtual = listaDePerguntas.get(idPergunta);
                adicionarQuestaoATela();
            } else {
                Intent intent = new Intent(PerguntaActivity.this, ResultadoActivity.class);

                Bundle b = new Bundle();
                b.putInt("acertos", respostasCorretas);
                b.putInt("fase", fase);
                b.putInt("respondidas", perguntasRespondidas);
                b.putStringArrayList("listaDeRespostas", listaDeRespostas);
                intent.putExtras(b);
                startActivity(intent);
                finish();

            }

        } else {
            Toast.makeText(getApplicationContext(), "Selecione uma alternativa!", Toast.LENGTH_SHORT).show();
        }
        grp.clearCheck();


    }

    @Override
    public void onBackPressed() {
        dialogConfirmacaoSaida().show();
    }

    private Dialog dialogConfirmacaoSaida() {
        return new AlertDialog.Builder(this).setTitle("Confirmação")
                .setMessage("Deseja realmente sair do jogo atual?")
                .setPositiveButton("Sim", confirmacaoPosisitiva())
                .setNegativeButton("Não", confirmacaoNegativa()).create();
    }

    private android.content.DialogInterface.OnClickListener confirmacaoPosisitiva() {
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        };
    }

    private android.content.DialogInterface.OnClickListener confirmacaoNegativa() {
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                return;
            }
        };
    }
}
