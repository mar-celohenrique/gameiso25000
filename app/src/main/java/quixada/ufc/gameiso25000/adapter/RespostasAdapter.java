package quixada.ufc.gameiso25000.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import quixada.ufc.gameiso25000.R;
import quixada.ufc.gameiso25000.model.Pergunta;

/**
 * Created by marcelo on 27/11/16.
 */

public class RespostasAdapter extends ArrayAdapter<HashMap<String, Object>> {

    LayoutInflater inflater;
    private ListView lvQsAns;

    private List<Pergunta> questionsList;
    private Pergunta currentQuestion;

    ArrayList<HashMap<String, Object>> originalValues = new ArrayList<HashMap<String, Object>>();


    public static String KEY_QUES = "questions";
    public static String KEY_CANS = "canswer";
    public static String KEY_YANS = "yanswer";


    public RespostasAdapter(Context context, int textViewResourceId,
                         ArrayList<HashMap<String, Object>> Strings) {
        super(context, textViewResourceId, Strings);
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    // class for caching the views in a row
    private class ViewHolder {

        TextView tvQS, tvCans, tvYouans;

    }

    ViewHolder viewHolder;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {

            convertView = inflater.inflate(R.layout.linhas_lista_respostas, null);
            viewHolder = new ViewHolder();

            viewHolder.tvQS = (TextView) convertView.findViewById(R.id.tvTextoPerguntas);

            viewHolder.tvCans = (TextView) convertView
                    .findViewById(R.id.tvItemCorreto);
            viewHolder.tvYouans = (TextView) convertView
                    .findViewById(R.id.tvRespostaJogador);

            // link the cached views to the convertview
            convertView.setTag(viewHolder);

        } else
            viewHolder = (ViewHolder) convertView.getTag();

        viewHolder.tvQS.setText(originalValues.get(position).get(KEY_QUES)
                .toString());

        viewHolder.tvCans.setText("Correct Ans: "+originalValues.get(position).get(KEY_CANS)
                .toString());
        viewHolder.tvYouans.setText("Your Ans: "+originalValues.get(position)
                .get(KEY_YANS).toString());

        // return the view to be displayed
        return convertView;
    }
}
