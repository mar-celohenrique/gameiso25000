package quixada.ufc.gameiso25000.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import quixada.ufc.gameiso25000.model.Caracteristica;
import quixada.ufc.gameiso25000.model.Fase;
import quixada.ufc.gameiso25000.model.Pergunta;
import quixada.ufc.gameiso25000.model.Subcaracteristica;

/**
 * Created by marcelo on 04/11/16.
 */

public class BaseDeDadosPerguntas extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 22;

    // Database Name
    private static final String DATABASE_NAME = "gameIso25000";

    //Tabela fase
    private static final String TABELA_FASE = "fase";

    //Colunas da tabela fase
    private static final String ID_FASE = "id";
    private static final String NOME_FASE = "nome";
    private static final String COMPLETA = "completa";

    // Tabela pergunta
    private static final String TABELA_PERGUNTA = "pergunta";

    // Colunas da tabela pergunta
    private static final String ID_PERGUNTA = "id";
    private static final String TEXTO_PERGUNTA = "texto";
    private static final String RESPOSTA_PERGUNTA = "resposta";
    private static final String ITEMA = "item_a";
    private static final String ITEMB = "item_b";
    private static final String ITEMC = "item_c";
    private static final String ITEMD = "item_d";
    private static final String ID_FASE_PERGUNTA = "id_fase";

    //Tabela caracteristica
    private static final String TABELA_CARACTERISTICA = "caracteristica";

    //Colunas tabela caracteristica
    private static final String ID_CARACTERISTICA = "id";
    private static final String NOME_CARACTERISTICA = "nome";
    private static final String ID_FASE_CARACTERISTICA = "id_fase";

    //Tabela subcaracteristica
    private static final String TABELA_SUBCARACTERISTICA = "subcaracteristica";

    //Colunas tabelta subcaracteristica
    private static final String ID_SUBCARACTERISTICA = "id";
    private static final String NOME_SUBCARACTERISTICA = "nome";
    private static final String ID_SUBCARACTERISTICA_CARACTERISTICA = "id_caracteristica";

    private SQLiteDatabase baseDeDados;


    public BaseDeDadosPerguntas(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        baseDeDados = db;
        String sqlFase = "CREATE TABLE IF NOT EXISTS " + TABELA_FASE + " ( "
                + ID_FASE + " INTEGER PRIMARY KEY AUTOINCREMENT, " + NOME_FASE
                + " TEXT, " + COMPLETA + " BOOLEAN)";
        baseDeDados.execSQL(sqlFase);


        String sqlPergunta = "CREATE TABLE IF NOT EXISTS " + TABELA_PERGUNTA + " ( "
                + ID_PERGUNTA + " INTEGER PRIMARY KEY AUTOINCREMENT, " + TEXTO_PERGUNTA
                + " TEXT, " + RESPOSTA_PERGUNTA + " TEXT, " + ITEMA + " TEXT, "
                + ITEMB + " TEXT, " + ITEMC + " TEXT, " + ITEMD + " TEXT," + ID_FASE_PERGUNTA +
                " REFERENCES " + NOME_FASE + " (" + ID_FASE + "))";
        baseDeDados.execSQL(sqlPergunta);

        String sqlCaracteristica = "CREATE TABLE IF NOT EXISTS " + TABELA_CARACTERISTICA + " ( "
                + ID_CARACTERISTICA + " INTEGER PRIMARY KEY AUTOINCREMENT, " + NOME_CARACTERISTICA
                + " TEXT, " + ID_FASE_CARACTERISTICA + " REFERENCES " + NOME_FASE + " (" + ID_FASE + "))";
        baseDeDados.execSQL(sqlCaracteristica);

        String sqlSubcaracteristica = "CREATE TABLE IF NOT EXISTS " + TABELA_SUBCARACTERISTICA + " ( "
                + ID_SUBCARACTERISTICA + " INTEGER PRIMARY KEY AUTOINCREMENT, " + NOME_SUBCARACTERISTICA
                + " TEXT, " + ID_SUBCARACTERISTICA_CARACTERISTICA + " REFERENCES " + NOME_CARACTERISTICA + " (" + ID_CARACTERISTICA + "))";
        baseDeDados.execSQL(sqlSubcaracteristica);
        popularBaseDeDados();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABELA_PERGUNTA);
        db.execSQL("DROP TABLE IF EXISTS " + TABELA_FASE);
        db.execSQL("DROP TABLE IF EXISTS " + TABELA_CARACTERISTICA);
        db.execSQL("DROP TABLE IF EXISTS " + TABELA_SUBCARACTERISTICA);
        onCreate(db);
    }

    public int contarNumeroPerguntas(int idFase) {
        int n = 0;
        String selectQuery = "SELECT  * FROM " + TABELA_PERGUNTA + " WHERE " + ID_FASE_PERGUNTA + " = " + idFase + "";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        n = cursor.getCount();
        return n;
    }

    public int contarNumeroCaracteristicas() {
        int n = 0;
        String selectQuery = "SELECT  * FROM " + TABELA_CARACTERISTICA + "";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        n = cursor.getCount();
        return n;
    }

    public List<Caracteristica> listarCaracteristicas() {
        List<Caracteristica> lista = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + TABELA_CARACTERISTICA + "";
        baseDeDados = this.getReadableDatabase();
        Cursor cursor = baseDeDados.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                Caracteristica c = new Caracteristica();
                c.setId(cursor.getInt(0));
                c.setNome(cursor.getString(1));
                c.setId_fase(cursor.getInt(2));
                lista.add(c);

            } while (cursor.moveToNext());
        }

        return lista;
    }

    public Caracteristica buscarCaracteristica(int id) {
        String selectQuery = "SELECT  * FROM " + TABELA_CARACTERISTICA + " WHERE " + ID_CARACTERISTICA + " = " + id + "";
        baseDeDados = this.getReadableDatabase();
        Cursor cursor = baseDeDados.rawQuery(selectQuery, null);
        Caracteristica c = null;
        if (cursor.moveToFirst()) {
            do {
                c = new Caracteristica();
                c.setId(cursor.getInt(0));
                c.setNome(cursor.getString(1));
                c.setId_fase(cursor.getInt(2));

            } while (cursor.moveToNext());
        }
        return c;
    }

    public List<Subcaracteristica> listarSubcaracteristicasPertencentesCaracteristica(int idCaracteristica) {
        List<Subcaracteristica> lista = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + TABELA_SUBCARACTERISTICA + " WHERE " + ID_SUBCARACTERISTICA_CARACTERISTICA + " = " + idCaracteristica + "";
        baseDeDados = this.getReadableDatabase();
        Cursor cursor = baseDeDados.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                Subcaracteristica s = new Subcaracteristica();
                s.setId(cursor.getInt(0));
                s.setNome(cursor.getString(1));
                s.setId_caracteristica(cursor.getInt(2));

                lista.add(s);

            } while (cursor.moveToNext());
        }

        return lista;
    }

    public List<Subcaracteristica> listarSubcaracteristicas() {
        List<Subcaracteristica> lista = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + TABELA_SUBCARACTERISTICA + "";
        baseDeDados = this.getReadableDatabase();
        Cursor cursor = baseDeDados.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                Subcaracteristica s = new Subcaracteristica();
                s.setId(cursor.getInt(0));
                s.setNome(cursor.getString(1));
                s.setId_caracteristica(cursor.getInt(2));

                lista.add(s);

            } while (cursor.moveToNext());
        }

        return lista;
    }

    public String converterListaParaString(List<Subcaracteristica> subcaracteristicas) {
        String[] lista = new String[subcaracteristicas.size()];
        for (int i = 0; i < lista.length; i++) {
            lista[i] = subcaracteristicas.get(i).getNome();
        }
        StringBuilder builder = new StringBuilder();
        for (String value : lista) {
            builder.append(value);
            builder.append(" ");
        }
        return builder.toString();
    }

    public List<Subcaracteristica> listarSubcaracteristicasNaoPertencentesCaracteristica(int idCaracteristica) {
        List<Subcaracteristica> lista = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + TABELA_SUBCARACTERISTICA + "";
        baseDeDados = this.getReadableDatabase();
        Cursor cursor = baseDeDados.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                if (cursor.getInt(2) != idCaracteristica) {
                    Subcaracteristica s = new Subcaracteristica();
                    s.setId(cursor.getInt(0));
                    s.setNome(cursor.getString(1));
                    s.setId_caracteristica(cursor.getInt(2));
                    lista.add(s);
                }

            } while (cursor.moveToNext());
        }

        return lista;
    }

    public List<Pergunta> listarTodasPerguntasFase(int idFase) {

        List<Pergunta> listadePerguntas = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABELA_PERGUNTA + " WHERE " + ID_FASE_PERGUNTA + " = " + idFase + "";
        baseDeDados = this.getReadableDatabase();

        Cursor cursor = baseDeDados.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Pergunta p = new Pergunta();
                p.setId(cursor.getInt(0));
                p.setTexto(cursor.getString(1));
                p.setResposta(cursor.getString(6));
                p.setItemA(cursor.getString(3));
                p.setItemB(cursor.getString(4));
                p.setItemC(cursor.getString(5));
                p.setItemD(cursor.getString(2));
                p.setIdFase(cursor.getInt(7));

                listadePerguntas.add(p);

            } while (cursor.moveToNext());
        }
        // return quest list
        return listadePerguntas;
    }

    public void adicionarPerguntaBaseDeDados(List<Pergunta> perguntas) {

        for (Pergunta pergunta : perguntas) {
            ContentValues valores = new ContentValues();
            valores.put(TEXTO_PERGUNTA, pergunta.getTexto());
            valores.put(RESPOSTA_PERGUNTA, pergunta.getResposta());
            valores.put(ITEMA, pergunta.getItemA());
            valores.put(ITEMB, pergunta.getItemB());
            valores.put(ITEMC, pergunta.getItemC());
            valores.put(ITEMD, pergunta.getItemD());
            valores.put(ID_FASE_PERGUNTA, pergunta.getIdFase());
            baseDeDados.insert(TABELA_PERGUNTA, null, valores);
        }

    }

    public void adicionarFaseBaseDeDados(List<Fase> fases) {
        for (Fase fase : fases) {
            ContentValues valores = new ContentValues();
            valores.put(ID_FASE, fase.getId());
            valores.put(NOME_FASE, fase.getNome());
            valores.put(COMPLETA, fase.isCompleta());

            baseDeDados.insert(TABELA_FASE, null, valores);
        }
    }

    public void adicionarCaracteristica(List<Caracteristica> caracteristicas) {
        for (Caracteristica c : caracteristicas) {
            ContentValues values = new ContentValues();
            values.put(ID_CARACTERISTICA, c.getId());
            values.put(NOME_CARACTERISTICA, c.getNome());
            values.put(ID_FASE_CARACTERISTICA, c.getId_fase());
            baseDeDados.insert(TABELA_CARACTERISTICA, null, values);
        }
    }

    public void adicionarSubcaracteristica(List<Subcaracteristica> subcaracteristicas) {
        for (Subcaracteristica s : subcaracteristicas) {
            ContentValues values = new ContentValues();
            values.put(ID_SUBCARACTERISTICA, s.getId());
            values.put(NOME_SUBCARACTERISTICA, s.getNome());
            values.put(ID_SUBCARACTERISTICA_CARACTERISTICA, s.getId_caracteristica());
            baseDeDados.insert(TABELA_SUBCARACTERISTICA, null, values);
        }
    }

    private void popularBaseDeDados() {
        List<Pergunta> perguntas = new ArrayList<>();
        List<Fase> fases = new ArrayList<>();
        List<Caracteristica> caracteristicas = new ArrayList<>();
        List<Subcaracteristica> subcaracteristicas = new ArrayList<>();
        Pergunta p1 = new Pergunta(1, "A norma ISO 25000 é voltada a empresas:", "Todos os portes", "De grande porte", " De grande e médio porte", "Somente de pequeno porte", "Todos os portes", 1);
        Pergunta p2 = new Pergunta(2, "Qual item abaixo está relacionado com Portabilidade definida na ISO/IEC 25000?", "Adaptabilidade", " Satisfação", "Testabilidade", "Segurança", "Adaptabilidade", 3);
        Pergunta p3 = new Pergunta(3, "Entre os itens a seguir, qual deles descreve a forma de divisão da série ISO/IEC 25000.", "Gestão da Qualidade, Modelo de Qualidade, Medição, Requisitos de Qualidade e Avaliação da Qualidade.", "Gestão da Qualidade, Modelo de Produto, Medição, Resquisitos de Produto e Avaliação do Produto.", "Gestão de Produto, Modelo de Produto, Medição, Requisitos de Qualidade e Avaliação da Qualidade.", "Gestão de Produto, Modelo de Qualidade, Medição, Requisitos de", "Gestão da Qualidade, Modelo de Qualidade, Medição, Requisitos de Qualidade e Avaliação da Qualidade.", 1);
        Pergunta p4 = new Pergunta(4, "Sabendo que a ISO/IEC 25000 possui três categorias de qualidade sendo elas qualidade interna, qualidade externa e qualidade em uso, escolha uma das opções abaixo que indique as categorias da qualidade interna e externa (Qualidade de Produto).", "Funcionalidade, Confiança, Usabilidade, Eficiência, Manutenção, Portabilidade, Segurança, Compatibilidade.", "Funcionalidade, Confiabilidade, Eficiência, Manutenibilidade, Portabilidade, Segurança, Compatibilidade", "Adequação Funcional, Confiabilidade, Usabilidade, Eficiência de Desempenho, Manutenibilidade, Portabilidade, Segurança, Compatibilidade.", "Adequação Funcional, Confiabilidade, Usabilidade, Eficiência de Desempenho, Manutenibilidade, Portabilidade, Segurança, Interoperabilidade.", "Adequação Funcional, Confiabilidade, Usabilidade, Eficiência de Desempenho, Manutenibilidade, Portabilidade, Segurança, Compatibilidade.", 1);
        Pergunta p5 = new Pergunta(5, "Sobre a característica Adequação Funcional escolha a opção mais adequada.", "Grau em que o produto ou sistema fornece funções que correspondem à algumas necessidades quando utilizado.", " Grau em que o produto ou sistema está de acordo com as funções que foram solicitados.", "Grau em que o produto ou sistema realiza as funções que correspondem as necessidades explicitas e implicitas em qualquer condição de uso.", "Grau em que o produto ou sistema fornece funções que correspondam às necessidades explícitas e ímplicitas quando utilizado sob as coondições especificadas.", "Grau em que o produto ou sistema fornece funções que correspondam às necessidades explícitas e ímplicitas quando utilizado sob as coondições especificadas.", 3);
        Pergunta p6 = new Pergunta(6, "A adequação Funcional é um caracteristica da qualidade de produto, sendo assim ela possui subcaracterisiticas que lhe compõem. Escolha a opção que indique quais as subcaracteristicas da Adequação Funcional.", "Função Completa, Corretude Funcional, Funcionalidade Inapropriada.", "Completude Funcional, Corretude Funcional, Funcionalidade Apropriada.", "Completude Funcional, Função Correta, Funcionalidade Apropriada.", "Função Completa, Corretude Funcional, Funcionalidade Apropriada.", "Completude Funcional, Corretude Funcional, Funcionalidade Apropriada.", 3);
        Pergunta p7 = new Pergunta(7, "Sobre a caracteristica Confiabilidade escolha a opção mais adequada.", "Grau em que um sistema, produto ou componente mantém, ao longo do tempo, um comportamento consistente com o esperado, sob as condições especificadas.", "Grau em que um sistema, produto ou componente mantém, ao longo do tempo, um comportamento inesperado, sob condições especificadas.", "Grau em que um sistema, produto ou componente mantém, ao longo do tempo, um comportamento consistente com o esperado, sob qualquer condição de uso.", "Grau em que um sistema, produto ou componente mantém, comportamentos inesperados ao longo do tempo especificado sobre algumas condições de uso.", "Grau em que um sistema, produto ou componente mantém, ao longo do tempo, um comportamento consistente com o esperado, sob as condições especificadas.", 3);
        Pergunta p8 = new Pergunta(8, "A Confiabilidade é uma caracteristica da qualidade de produto, sendo assim ela possui subcaracterisiticas que lhe compõem. Escolha a opção que indique quais as subcaracteristicas da Confiabilidade.", "Maturidade, Tolerância a Falhas, Consistência, Disponibilidade.", "Maturidade, Tolerância a Erros, Consistência, Disponibilidade.", "Satisfação, Tolerância a Erros, Recuperabilidade, Acessibilidade.", "Maturidade, Tolerância a Falhas, Recuperabilidade, Disponibilidade", "Maturidade, Tolerância a Falhas, Recuperabilidade, Disponibilidade", 3);
        Pergunta p9 = new Pergunta(9, "Sobre a caracteristica Usabilidade escolha a opção mais adequada.", "Grau em que um produto ou sistema pode ser utilizado por usuários específicos para atingir metas especificadas com eficácia, eficiência e satisfação, em um determinado contexto.", "Grau em que um produto ou sistema pode ser utilizado por quaisquer usuários para atingir metas especificadas com eficácia, eficiência e satisfação, em qualquer contexto.", "Grau em que um produto ou sistema pode ser utilizado por usuários específicos para atingir metas especificadas sem eficácia, eficiência e satisfação, em um determinado contexto.", "Grau em que um produto ou sistema pode ser utilizado por quaisquer usuários para atingir quaisquer metas com eficácia, eficiência e satisfação, em um determinado contexto.", "Grau em que um produto ou sistema pode ser utilizado por usuários específicos para atingir metas especificadas com eficácia, eficiência e satisfação, em um determinado contexto.", 3);
        Pergunta p10 = new Pergunta(10, "A Usabilidade é uma caracteristica da qualidade de produto, sendo assim ela possui subcaracterisiticas que lhe compõem. Escolha a opção que indique quais as subcaracteristicas da Usabilidade.", "Conhecimento, Apreensibilidade, Facilidade, Acessibilidade, Proteção de Erros, Estética da Interface.", "Conhecimento Adequado, Eficácia, Operabilidade, Acessabilidade, Proteção de Erros, Interação Agradável.", "Conhecimento Adequado, Apreensibilidade, Operabilidade, Acessibilidade, Proteção de Erro do usuário, Estética da Interface.", "Conhecimento, Eficácia, Facilidade, Acessabilidade, Proteção de Erros, Interação Agradável.", "Conhecimento Adequado, Apreensibilidade, Operabilidade, Acessibilidade, Proteção de Erro do usuário, Estética da Interface.", 3);
        Pergunta p11 = new Pergunta(11, "Sobre a caracteristica Eficiência de Desempenho escolha a opção mais adequada.", "Desempenho do produto em relação à quantidade de funções utilizados sob quaisquer condições.", "Desempenho do produto em relação à quantidade dos recursos utilizados sob condições estabelicidas.", "Desempenho do produto em relação à quantidade dos recursos utilizados sob quaisquer condições.", "Desempenho do produto em relação à quantidade de funções utilizados sob condições estabelicidas.", "Desempenho do produto em relação à quantidade dos recursos utilizados sob condições estabelicidas.", 3);
        Pergunta p12 = new Pergunta(12, "A Eficiência de Desempenho é uma caracteristica da qualidade de produto, sendo assim ela possui subcaracterisiticas que lhe compõem. Escolha a opção que indique quais as subcaracteristicas da Eficiência de Desempenho.", "Comportamento no Tempo, Utilização de Funções, Capacidade.", "Comportamento de Recurso, Utilização de Funções, Capacidade.", "Comportamento de Recurso, Utilização de Recursos, Capacidade.", "Comportamento no Tempo, Utilização de Recursos, Capacidade.", "Comportamento no Tempo, Utilização de Recursos, Capacidade.", 3);
        Pergunta p13 = new Pergunta(13, "Sobre a caracteristica Manutenibilidade escolha a opção mais adequada.", "Grau de facilidade com que um produto ou sistema pode ser modificado pela equipe de manutenção.", "Grau de eficácia e eficiência com que um produto ou sistema pode ser modificado pela equipe de teste.", "Grau de eficácia e eficiência com que um produto ou sistema pode ser modificado pela equipe de manutenção.", "Grau de facilidade com que um produto ou sistema pode ser modificado pela equipe de teste.", "Grau de eficácia e eficiência com que um produto ou sistema pode ser modificado pela equipe de manutenção.", 3);
        Pergunta p14 = new Pergunta(14, "A Manutenabilidade é uma caracteristica da qualidade de produto, sendo assim ela possui subcaracterisiticas que lhe compõem. Escolha a opção que indique quais as subcaracteristicas da Manutenabilidade.", "Analisabilidade, Modificabilidade, Modularidade, Reusabilidade, Testabilidade.", "Analisabilidade, Modificação, Modularidade, Reutilizavel, Testabilidade.", "Avaliabilidade, Modificabilidade, Modularidade, Reutilizavel, Testabilidade", "Avaliabilidade, Modificabilidade, Modularidade, Reusabilidade, Testabilidade", "Analisabilidade, Modificabilidade, Modularidade, Reusabilidade, Testabilidade.", 3);
        Pergunta p15 = new Pergunta(15, "Sabendo que a ISO/IEC 25000 possui três categorias de qualidade sendo elas qualidade interna, qualidade externa e qualidade em uso, escolha uma das opções abaixo que indique as categorias da qualidade em Uso.", "Efetividade, Eficácia, Utilidade, Inexistência de Risco, Flexibilidade.", "Efetividade, Eficiência, Utilidade, Inexistência de Risco, Flexibilidade.", "Efetividade, Eficiência, Satisfação, Inexistência de Risco, Cobertura de Contexto", "Acurácia, Eficácia, Satisfação, Mitigação, Cobertura do Contexto.", "Efetividade, Eficiência, Satisfação, Inexistência de Risco, Cobertura de Contexto", 3);
        Pergunta p16 = new Pergunta(16, "Sobre a característica Efetividade escolha a opção mais adequada.", "Acurácia e completude com a qual os usuários alcançam as metas específicas.", "Acurácia com a qual os usuários terminam suas tarefas.", "Completude com a qual os usuários traçam as metas específicas.", "Acurácia e completude com a qual os usuários traçam as metas específicas.", "Acurácia e completude com a qual os usuários alcançam as metas específicas.", 3);
        Pergunta p17 = new Pergunta(17, "Sobre a característica Eficiência escolha a opção mais adequada.", "Recursos usados em relação à completude com a qual os usuários alcançam as metas.", "Recursos gastos em relação à acurácia com a qual os usuários realizam suas metas.", "Recursos usados em relação à acurácia e completude com a qual usuários realizam suas metas.", "Recursos gastos em relação à acurácia e completude com a qual usuários alcançam as metas.", " Recursos gastos em relação à acurácia e completude com a qual usuários alcançam as metas.", 3);
        Pergunta p18 = new Pergunta(18, "Sobre a característica Satisfação escolha a opção mais adequada.", "Grau em que as necessidades dos usuários estão satisfeitas quando um produto ou sistema é usado em qualquer contexto.", "Grau em que as necessidades dos usuários estão satisfeitas quando um produto ou sistema é usado em um contexto de uso especificado.", "Grau em que as necessidades dos usuários não estão satisfeitas quando um produto ou sistema é usado em qualquer contexto.", "Grau em que as necessidades dos usuários não estão satisfeitas quando um produto ou sistema é usado em um contexto de uso especificado.", "Grau em que as necessidades dos usuários estão satisfeitas quando um produto ou sistema é usado em um contexto de uso especificado.", 3);
        Pergunta p19 = new Pergunta(19, "A Satisfação é uma caracteristica da qualidade em uso, sendo assim ela possui subcaracterisiticas que lhe compõem. Escolha a opção que indique quais as subcaracteristicas da Satisfação.", "Utilidade, Confiante, Cumprimento de Necessidades, Conforto.", "Util, Confiança, Prazer, Conforto Físico.", "Util, Confiante, Cumprimento de Necessidades, Conforto Físico.", "Utilidade, Confiança, Prazer, Conforto.", "Utilidade, Confiança, Prazer, Conforto.", 3);
        Pergunta p20 = new Pergunta(20, "Sobre a característica Inexistência de Risco escolha a opção mais adequada.", "Grau em que um produto reduz o risco potencial de status econômico, a saúde ou ao meio ambiente.", "Grau em que um sistema reduz o risco potencial de status econômico, e vida humana.", "Grau em que um produto ou sistema reduz o risco potencial de, a vida humana, a saúde ou ao meio ambiente.", "Grau em que um produto ou sistema reduz o risco potencial de status econômico, a vida humana, a saúde ou ao meio ambiente.", "Grau em que um produto ou sistema reduz o risco potencial de status econômico, a vida humana, a saúde ou ao meio ambiente.", 3);
        Pergunta p21 = new Pergunta(21, "A Inexistência de Risco é uma caracteristica da qualidade em uso, sendo assim ela possui subcaracterisiticas que lhe compõem. Escolha a opção que indique quais as subcaracteristicas da Inexistência.", "Mitigação de risco económico, Mitigação do risco de saúde e segurança, Mitigação de riscos ambientais.", "Mitigação de riscos, Mitigação do risco de segurança, Mitigação de riscos ambientais.", "Mitigação de risco económico, Mitigação do risco de saúde, Mitigação de riscos de vida humana.", "Mitigação de risco económico, Mitigação do risco de saúde e segurança, Mitigação de riscos de vida humana.", "Mitigação de risco económico, Mitigação do risco de saúde e segurança, Mitigação de riscos ambientais.", 3);
        Pergunta p22 = new Pergunta(22, "Sobre a característica Cobertura do Contexto escolha a opção mais adequada.", "Grau ao qual um produto ou sistema pode ser utilizado com inexistência de risco e satisfação em ambos os contextos de utilização especificadas e em contextos além dos inicialmente identificados explicitamente.", "Grau ao qual um produto ou sistema pode ser utilizado com eficácia, eficiência, e inexistência de risco e satisfação em ambos os contextos de utilização especificadas e em contextos além dos inicialmente identificados explicitamente.", "Grau ao qual um produto ou sistema pode ser utilizado com eficácia, eficiência em ambos os contextos de utilização especificadas e em contextos além dos inicialmente identificados explicitamente.", "Grau ao qual um produto ou sistema pode ser utilizado com eficácia, eficiência, e inexistência de risco e satisfação em ambos os contextos de utilização especificadas.", "Grau ao qual um produto ou sistema pode ser utilizado com eficácia, eficiência, e inexistência de risco e satisfação em ambos os contextos de utilização especificadas e em contextos além dos inicialmente identificados explicitamente.", 3);
        Pergunta p23 = new Pergunta(23, "A Cobertura do Contexto é uma caracteristica da qualidade em uso, sendo assim ela possui subcaracterisiticas que lhe compõem. Escolha a opção que indique quais as subcaracteristicas da Cobertura de Contexto.", "Completude do contexto, Liberdade de Risco.", "Utilização Especifica, Flexivel.", "Completude do Contexto, Flexibilidade.", "Utilização Especifica, Liberdade de Risco.", "Completude do Contexto, Flexibilidade.", 3);
        Pergunta p24 = new Pergunta(24, "Medidas de software são formas de quantificar o esforço necessário para a construção de um sistema de software. Sobre medidas de software é correto afirmar que:", "Um indicador é parte de uma medida.", "Custo, funcionalidade e número de erros são medidas indiretas.", "A medida de pontos de função só pode ser calculada com o código terminado.", "Linhas de código, esforço e memória são medidas diretas.", "Linhas de código, esforço e memória são medidas diretas.", 3);
        Pergunta p25 = new Pergunta(25, "Medidas de adequação medem atributos do software que evidenciam um conjunto de funções e sua apropriação para as tarefas especificadas. Escolha a opção que refere-se a uma medida de adequação.", "Resolução de Falhas.", " Cobertura das Implementadas Funções.", "Entendimento da Entrada/Saída.", "Utilização da Memória.", "Cobertura das Implementadas Funções.", 3);
        Pergunta p26 = new Pergunta(26, "Medidas de maturidade medem atributos do software que evidenciam quão livre o software está de ocorrências de falhas internas.", "Resolução de Falhas.", "Cobertura das Implementadas Funções.", "Entendimento da Entrada/Saída.", "Utilização da Memória com falha.", "Resolução de Falhas.", 3);
        Pergunta p27 = new Pergunta(27, "Medidas de integibilidade medem os atributos que evidenciam o esforço do usuário para reconhecer o conceito lógico e sua aplicabilidade.", "Resolução de Falhas.", "Cobertura das Implementadas Funções.", "Entendimento da Entrada/Saída.", "Utilização da Memória com falha.", "Entendimento da Entrada/Saída.", 3);
        Pergunta p28 = new Pergunta(28, "Medidas de comportamento em relação aos recursos medem os atributos do software que evidenciam a quantidade de recursos usados e a duração de seu uso na execução de suas funções.", "Resolução de Falhas.", "Cobertura das Implementadas Funções.", "Entendimento da Entrada/Saída.", "Utilização da Memória.", "Utilização da Memória.", 3);
        Pergunta p29 = new Pergunta(29, "Medidas de capacidade para ser instalado medem os atributos do software que evidenciam o esforço necessário para sua instalação num ambiente especificado.", "Prevenção da corrupção dos dados.", "Não permissão de operações incorretas.", "Facilidade nas mudanças.", "Pouco esforço para instalação.", "Pouco esforço para instalação.", 3);
        Pergunta p30 = new Pergunta(30, "Medidas de modificabilidade medem os atributos do software que evidenciam o esforço necessário para modificá-lo, remover seus defeitos ou adaptá-lo a mudanças ambientais.", "Prevenção da corrupção dos dados.", " Não permissão de operações incorretas.", "Facilidade nas mudanças.", "Pouco esforço para instalação.", "Facilidade nas mudanças.", 3);
        Pergunta p31 = new Pergunta(31, "Medidas de segurança indicam um conjunto de atributos para avaliar a capacidade do produto de software de evitar acesso ilegal ao sistema e a seus dados.", "Prevenção da corrupção dos dados.", "Não permissão de operações incorretas nos dados.", "Facilidade nas mudanças.", "Pouco esforço para instalação.", "Prevenção da corrupção dos dados.", 3);
        Pergunta p32 = new Pergunta(32, "Medidas de tolerância a falhas indicam um conjunto de atributos para avaliar a capacidade do produto de software de manter um nível de performance desejável em caso de falhas operacionais ou uso não natural das interfaces.", "Prevenção da corrupção dos dados.", "Não permissão de operações incorretas.", "Facilidade nas mudanças.", "Pouco esforço para instalação.", "Não permissão de operações incorretas.", 3);
        Pergunta p33 = new Pergunta(33, "Medidas de aprendizado avaliam quanto tempo os usuários levarão para aprender a usar funcionalidades particulares do sistema e a eficácia dos sistemas de ajuda e documentação.", "Prevenção da corrupção dos dados.", "Completude da documentação.", "Completude da documentação para o usuário e facilidade de uso dos sistemas de ajuda.", "Utilização de Entrada e Saída.", "Completude da documentação para o usuário e facilidade de uso dos sistemas de ajuda.", 3);
        Pergunta p34 = new Pergunta(34, "Medidas de utilização de recursos indicam um conjunto de atributos para prever a utilização de recursos de hardware pelo sistema computacional incluindo o produto de software durantes os testes ou operação.", "Impacto da mudança.", "Completude da documentação.", "Completude da documentação para o usuário e facilidade de uso dos sistemas de ajuda.", "Utilização de Entrada e Saída.", "Utilização de Entrada e Saída.", 3);
        Pergunta p35 = new Pergunta(35, "Medidas de custo de mudança indicam um conjunto de atributos para prever o tempo de esforço necessário na tentativa de implementação de uma mudança no produto de software.", "Impacto da mudança.", "Completude da documentação.", "Adaptabilidade ao ambiente de hardware.", "Segurança", "Impacto da mudança.", 3);
        Pergunta p36 = new Pergunta(36, "Medidas de adaptabilidade Indicam um conjunto de atributos para prever o impacto que o produto de software pode ter no esforço do usuário ao tentar adaptá-lo a outros ambientes.", "Impacto da mudança.", "Completude da documentação.", "Adaptabilidade ao ambiente de hardware.", "Utilização de Entrada e Saída.", "Adaptabilidade ao ambiente de hardware.", 3);
        Pergunta p37 = new Pergunta(37, "Indique a opção que mostra as etapas do processo de avaliação da qualidade.", "Especificar o modelo de qualidade, Especificar a avaliação, Produzir o plano, Julgar os resultados.", "Estabelecer requisitos de avaliação, Especificar a avaliação, Projetar a avaliação, Executar a avaliação.", "Especificar o modelo de qualidade, Especificar a avaliação, Projetar a avaliação, Executar a avaliação.", "Estabelecer requisitos de avaliação, Especificar a avaliação, Produzir o plano, Executar a avaliação.", "Estabelecer requisitos de avaliação, Especificar a avaliação, Projetar a avaliação, Executar a avaliação.", 5);
        Pergunta p38 = new Pergunta(38, "Sobre	a	etapa,	Estabalecer requisitos de avaliação, indique qual a opção que refere-se ao que se deve ser feito nessa etapa.", "Estabelecer o próposito da avaliação, Identificar tipos de produtos a serem validados, Especificar o modelo de qualidade.", "Estabelecer requisitos da avaliação, Identificar tipos de produtos a serem validados, Especificar o modelo de qualidade.", "Estabelecer o próposito da avaliação, Identificar produtos, utilizar o modelo de qualidade.", "Estabelecer requisitos da avaliação, Identificar tipos de produtos a serem validados, utilizar o modelo de qualidade.", "Estabelecer o próposito da avaliação, Identificar tipos de produtos a serem validados, Especificar o modelo de qualidade.", 5);
        Pergunta p39 = new Pergunta(39, "Sobre a etapa, Especificar a avaliação, indique qual a opção que refere-se ao que se deve ser feito nessa etapa.", "Criar métricas, Estabelecer níveis de pontuação para as métricas, Estabelecer critérios para as métricas.", "Criar métricas, Estabelecer objetivos das métricas, Estabelecer critérios para julgamento.", "Selecionar métricas, Estabelecer objetivos para as métricas, Estabelecer critérios para as métricas.", "Selecionar métricas, Estabelecer níveis de pontuação para as métricas, Estabelecer critérios para julgamento.", "Selecionar métricas, Estabelecer níveis de pontuação para as métricas, Estabelecer critérios para julgamento.", 5);
        Pergunta p40 = new Pergunta(40, "Sobre a etapa, Projetar a avaliação, indique qual a opção que refere-se ao que se deve ser feito nessa etapa.", "Utilizar o Plano de Avaliação.", "Produzir o Plano de Métricas.", "Produzir o Plano de Avaliação.", "Utilizar o Plano de Métricas.", "Produzir o Plano de Avaliação.", 5);
        Pergunta p41 = new Pergunta(41, "Sobre a etapa, Projetar a avaliação, indique qual a opção que refere-se ao que se deve ser feito nessa etapa.", "Obter as Medidas, Comparar com Critérios, Julgar os Resultados.", "Estabelecer requisitos de avaliação, Especificar a avaliação, Projetar a avaliação, Executar a avaliação.", "Especificar o modelo de qualidade, Especificar a avaliação, Projetar a avaliação, Executar a avaliação.", "Estabelecer requisitos de avaliação, Especificar a avaliação, Produzir o plano, Executar a avaliação.", "Obter as Medidas, Comparar com Critérios, Julgar os Resultados.", 5);
        Pergunta p42 = new Pergunta(42, "Sobre a etapa, Executar avaliação, indique qual a opção que refere-se ao que se deve ser feito nessa etapa.", "Obter as Medidas, Comparar com Critérios, Julgar os Resultados.", "Utilizar as Medidas, Comparar os Resultados, Julgar os Resultados.", "Obter as Medidas, Comparar com Critérios, Comparar os Resultados.", "Utilizar as Medidas, Comparar os Resultados, Comparar os Resultados.", "Obter as Medidas, Comparar com Critérios, Julgar os Resultados.", 5);

        Pergunta p43 = new Pergunta(43, "Qual o significado de SQuaRE na iso 25000?", "Produtividade e eficacia na gestão de qualidade", "Requisitos de qualidade e avaliação de produtos de software", "Gerenciamento de qualidade externa e interna de software", "Requisitos de qualidade e Gerenciamento de medições", "Requisitos de qualidade e avaliação de produtos de software", 4);
        Pergunta p44 = new Pergunta(44, "Qual dessas divisões da iso 25000 é a de requisitos de qualidade?", "ISO/IEC 2502n", "ISO/IEC 2501n", "ISO/IEC 2504n", "ISO/IEC 2503n", "ISO/IEC 2503n", 4);
        Pergunta p45 = new Pergunta(45, "A norma SQuaRe tem como uma das premissas:", "Diversificar guias de qualidade", "Mudar as métricas de qualidade", "Normalizar os requisitos de qualidade", "Manter modelos de qualidade antigos", "Normalizar os requisitos de qualidade", 4);
        Pergunta p46 = new Pergunta(46,"Qual dessas divisões da iso 25000 é a de modelo de qualidade?", "ISO/IEC 2502n", "ISO/IEC 2501n", "ISO/IEC 2504n", "ISO/IEC 2503n", "ISO/IEC 2502n", 2);
        Pergunta p47 = new Pergunta(47, "A divisão de qualidade é responsável por:", "Definir um modelo de características de qualidade, descrevendo o que se espera de um produto.", "Fornecer uma estrutura destinada a identificação dos requisitos gerais", "Definir um módulo de avaliação capaz de avaliar erros induzidos ou detectados e a maneira como o sistema trata e recupera estes eventos", "Destinar a especificação dos requisitos de qualidade.", "Definir um modelo de características de qualidade, descrevendo o que se espera de um produto.", 2);

        perguntas.add(p1);
        perguntas.add(p2);
        perguntas.add(p3);
        perguntas.add(p4);
        perguntas.add(p5);
        perguntas.add(p6);
        perguntas.add(p7);
        perguntas.add(p8);
        perguntas.add(p9);
        perguntas.add(p10);
        perguntas.add(p11);
        perguntas.add(p12);
        perguntas.add(p13);
        perguntas.add(p14);
        perguntas.add(p15);
        perguntas.add(p16);
        perguntas.add(p17);
        perguntas.add(p18);
        perguntas.add(p19);
        perguntas.add(p20);
        perguntas.add(p21);
        perguntas.add(p22);
        perguntas.add(p23);
        perguntas.add(p24);
        perguntas.add(p25);
        perguntas.add(p26);
        perguntas.add(p27);
        perguntas.add(p28);
        perguntas.add(p29);
        perguntas.add(p30);
        perguntas.add(p31);
        perguntas.add(p32);
        perguntas.add(p33);
        perguntas.add(p34);
        perguntas.add(p35);
        perguntas.add(p36);
        perguntas.add(p37);
        perguntas.add(p38);
        perguntas.add(p39);
        perguntas.add(p40);
        perguntas.add(p41);
        perguntas.add(p42);
        perguntas.add(p43);
        perguntas.add(p44);
        perguntas.add(p45);
        perguntas.add(p46);
        perguntas.add(p47);

        adicionarPerguntaBaseDeDados(perguntas);

        Fase f1 = new Fase(1, "GESTÃO DE QUALIDADE", false);
        Fase f2 = new Fase(2, "MODELO DE QUALIDADE", false);
        Fase f3 = new Fase(3, "MEDIÇÃO DE QUALIDADE", false);
        Fase f4 = new Fase(4, "REQUISITOS DE QUALIDADE", false);
        Fase f5 = new Fase(5, "AVALIAÇÃO DE QUALIDADE", false);
        Fase f6 = new Fase(6, "CARACTERÍSTICAS", false);
        fases.add(f1);
        fases.add(f2);
        fases.add(f3);
        fases.add(f4);
        fases.add(f5);
        fases.add(f6);
        adicionarFaseBaseDeDados(fases);

        Caracteristica c1 = new Caracteristica(1, 6, "Compatibilidade");
        Caracteristica c2 = new Caracteristica(2, 6, "Portabilidade");
        Caracteristica c3 = new Caracteristica(3, 6, "Adequação Funcional");
        Caracteristica c4 = new Caracteristica(4, 6, "Eficiência de Desempenho");
        Caracteristica c5 = new Caracteristica(5, 6, "Usabilidade");
        Caracteristica c6 = new Caracteristica(6, 6, "Confiabilidade");
        Caracteristica c7 = new Caracteristica(7, 6, "Segurança");
        Caracteristica c8 = new Caracteristica(8, 6, "Manutenção");


        caracteristicas.add(c1);
        caracteristicas.add(c2);
        caracteristicas.add(c3);
        caracteristicas.add(c4);
        caracteristicas.add(c5);
        caracteristicas.add(c6);
        caracteristicas.add(c7);
        caracteristicas.add(c8);
        adicionarCaracteristica(caracteristicas);

        Subcaracteristica s1 = new Subcaracteristica("Coexistência", 1, 1);
        Subcaracteristica s2 = new Subcaracteristica("Interoperabilidade", 2, 1);

        Subcaracteristica s3 = new Subcaracteristica("Adaptabilidade", 3, 2);
        Subcaracteristica s4 = new Subcaracteristica("Instabilidade", 4, 2);
        Subcaracteristica s5 = new Subcaracteristica("Substituição", 5, 2);


        Subcaracteristica s6 = new Subcaracteristica("Completude Funcional", 6, 3);
        Subcaracteristica s7 = new Subcaracteristica("Exatidão Funcional", 7, 3);
        Subcaracteristica s8 = new Subcaracteristica("Adequação Apropriada", 8, 3);


        Subcaracteristica s9 = new Subcaracteristica("Tempo de Comportamento", 9, 4);
        Subcaracteristica s10 = new Subcaracteristica("Utilização de Recursos", 10, 4);
        Subcaracteristica s11 = new Subcaracteristica("Capacidade", 11, 4);

        Subcaracteristica s12 = new Subcaracteristica("Reconhecibilidade de Adequação", 12, 5);
        Subcaracteristica s13 = new Subcaracteristica("Aprendizado", 13, 5);
        Subcaracteristica s14 = new Subcaracteristica("Operabilidade", 14, 5);
        Subcaracteristica s15 = new Subcaracteristica("Proteção de erro ao usuário", 15, 5);
        Subcaracteristica s16 = new Subcaracteristica("Estética da interface de usuário", 16, 5);
        Subcaracteristica s17 = new Subcaracteristica("Acessibilidade", 17, 5);

        Subcaracteristica s18 = new Subcaracteristica("Maturidade", 18, 6);
        Subcaracteristica s19 = new Subcaracteristica("Disponibilidade", 19, 6);
        Subcaracteristica s20 = new Subcaracteristica("Tolerância a falhas", 20, 6);
        Subcaracteristica s21 = new Subcaracteristica("Recuperabilidade", 21, 6);

        Subcaracteristica s22 = new Subcaracteristica("Confidencialidade", 22, 7);
        Subcaracteristica s23 = new Subcaracteristica("Integridade", 23, 7);
        Subcaracteristica s24 = new Subcaracteristica("Não-repudio", 24, 7);
        Subcaracteristica s25 = new Subcaracteristica("Prestação de Contas", 25, 7);
        Subcaracteristica s26 = new Subcaracteristica("Autenticidade", 26, 7);

        Subcaracteristica s27 = new Subcaracteristica("Modularidade", 27, 8);
        Subcaracteristica s28 = new Subcaracteristica("Reusabilidade", 28, 8);
        Subcaracteristica s29 = new Subcaracteristica("Possibilidade de análise", 29, 8);
        Subcaracteristica s30 = new Subcaracteristica("Modificabilidade", 30, 8);
        Subcaracteristica s31 = new Subcaracteristica("Testabilidade", 31, 8);


        subcaracteristicas.add(s1);
        subcaracteristicas.add(s2);
        subcaracteristicas.add(s3);
        subcaracteristicas.add(s4);
        subcaracteristicas.add(s5);
        subcaracteristicas.add(s6);
        subcaracteristicas.add(s7);
        subcaracteristicas.add(s8);
        subcaracteristicas.add(s9);
        subcaracteristicas.add(s10);
        subcaracteristicas.add(s11);
        subcaracteristicas.add(s12);
        subcaracteristicas.add(s13);
        subcaracteristicas.add(s14);
        subcaracteristicas.add(s15);
        subcaracteristicas.add(s16);
        subcaracteristicas.add(s17);
        subcaracteristicas.add(s18);
        subcaracteristicas.add(s19);
        subcaracteristicas.add(s20);
        subcaracteristicas.add(s21);
        subcaracteristicas.add(s22);
        subcaracteristicas.add(s23);
        subcaracteristicas.add(s24);
        subcaracteristicas.add(s25);
        subcaracteristicas.add(s26);
        subcaracteristicas.add(s27);
        subcaracteristicas.add(s28);
        subcaracteristicas.add(s29);
        subcaracteristicas.add(s30);
        subcaracteristicas.add(s31);


        adicionarSubcaracteristica(subcaracteristicas);


    }

    public List<Fase> listarFases() {
        List<Fase> fases = new ArrayList<>();

        String selectQuery = "SELECT  * FROM " + TABELA_FASE + "";
        baseDeDados = this.getReadableDatabase();

        Cursor cursor = baseDeDados.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                Fase f = new Fase();
                f.setId(cursor.getInt(0));
                f.setNome(cursor.getString(1));
                f.setCompleta(cursor.getInt(2) > 0);

                fases.add(f);

            } while (cursor.moveToNext());
        }

        return fases;
    }

    public void atualizarFase(boolean completa, int id) {
        ContentValues cv = new ContentValues();
        cv.put(COMPLETA, completa);
        baseDeDados = this.getReadableDatabase();

        baseDeDados.update(TABELA_FASE, cv, "id = " + id, null);
    }

    public List<Subcaracteristica> sortearSubcaracteristicas(List<Subcaracteristica> subcaracteristicas) {
        Collections.shuffle(subcaracteristicas);
        List<Subcaracteristica> sorteadas = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            sorteadas.add(subcaracteristicas.get(i));
        }
        return sorteadas;
    }


}
