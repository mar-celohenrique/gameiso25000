package quixada.ufc.gameiso25000.model;

import java.util.List;

/**
 * Created by marcelo on 04/11/16.
 */

public class Fase {
    private int id;
    private String nome;
    private boolean completa;

    public Fase(){
        this.id = 0;
        this.nome = "";
        this.completa = false;
    }

    public Fase(int id, String nome, boolean completa) {
        this.id = id;
        this.nome = nome;
        this.completa = completa;
    }

    public boolean isCompleta() {
        return completa;
    }

    public void setCompleta(boolean completa) {
        this.completa = completa;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}

