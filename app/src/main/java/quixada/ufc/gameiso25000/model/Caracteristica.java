package quixada.ufc.gameiso25000.model;

/**
 * Created by marcelo on 27/11/16.
 */

public class Caracteristica {

    private int id;
    private String nome;
    private int id_fase;

    public int getId_fase() {
        return id_fase;
    }

    public void setId_fase(int id_fase) {
        this.id_fase = id_fase;
    }

    public Caracteristica(){}

    public Caracteristica(int id, int id_fase, String nome) {
        this.id = id;
        this.id_fase = id_fase;
        this.nome = nome;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
