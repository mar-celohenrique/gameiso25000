package quixada.ufc.gameiso25000.model;

/**
 * Created by marcelo on 27/11/16.
 */

public class Subcaracteristica {

    private String nome;
    private int id;
    private int id_caracteristica;

    public Subcaracteristica(){}

    public Subcaracteristica(String nome, int id, int id_caracteristica) {
        this.nome = nome;
        this.id = id;
        this.id_caracteristica = id_caracteristica;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_caracteristica() {
        return id_caracteristica;
    }

    public void setId_caracteristica(int id_caracteristica) {
        this.id_caracteristica = id_caracteristica;
    }
}
