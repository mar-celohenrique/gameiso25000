package quixada.ufc.gameiso25000.model;

/**
 * Created by marcelo on 03/11/16.
 */

public class Pergunta {


    private int id;
    private String texto;
    private String resposta;
    private String itemA;
    private String itemB;
    private String itemC;
    private String itemD;
    private int idFase;

    public Pergunta() {
        this.id = 0;
        this.texto = "";
        this.resposta = "";
        this.itemA = "";
        this.itemB = "";
        this.itemC = "";
        this.itemD = "";
        this.idFase = 0;
    }

    public Pergunta(int id, String texto, String resposta, String itemA, String itemB, String itemC, String itemD, int idFase) {
        this.id = id;
        this.texto = texto;
        this.resposta = resposta;
        this.itemA = itemA;
        this.itemB = itemB;
        this.itemC = itemC;
        this.itemD = itemD;
        this.idFase = idFase;
    }
    public String getTexto() {
        return texto;
    }

    public int getIdFase() {
        return idFase;
    }

    public void setIdFase(int idFase) {
        this.idFase = idFase;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getResposta() {
        return resposta;
    }

    public void setResposta(String resposta) {
        this.resposta = resposta;
    }

    public String getItemA() {
        return itemA;
    }

    public void setItemA(String itemA) {
        this.itemA = itemA;
    }

    public String getItemB() {
        return itemB;
    }

    public void setItemB(String itemB) {
        this.itemB = itemB;
    }

    public String getItemC() {
        return itemC;
    }

    public void setItemC(String itemC) {
        this.itemC = itemC;
    }

    public String getItemD() {
        return itemD;
    }

    public void setItemD(String itemD) {
        this.itemD = itemD;
    }
}
